/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <../include/debug_utilities.h>
#include <../include/L1cache.h>
#include <../include/L2cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int l1_l2_entry_info_get (const struct cache_params l1_params,
                        const struct cache_params l2_params,
			  			long address,
                        entry_info* l1_l2_info,
			  			bool debug)
{
	(*l1_l2_info).original_address = address;

	int L1_offset_bits = log2(l1_params.block_size);
	int L1_idx_bits = log2((l1_params.size*KB)/(l1_params.block_size*l1_params.asociativity));
	int L1_tag_bits = ADDRSIZE - L1_offset_bits - L1_idx_bits;

	long L1_idx_mask = (1<<L1_idx_bits)-1;
	(*l1_l2_info).l1_idx = (address>>L1_offset_bits)&L1_idx_mask;
	long L1_tag_mask = (1<<L1_tag_bits)-1;
	(*l1_l2_info).l1_tag = (address>>(L1_offset_bits + L1_idx_bits))&L1_tag_mask;
	(*l1_l2_info).l1_assoc = l1_params.asociativity;

	int L2_offset_bits = log2(l2_params.block_size);
	int L2_idx_bits = log2((l2_params.size*KB)/(l2_params.block_size*l2_params.asociativity));
	int L2_tag_bits = ADDRSIZE - L2_offset_bits - L2_idx_bits;

	long L2_idx_mask = (1<<L2_idx_bits)-1;
	(*l1_l2_info).l2_idx = (address>>L2_offset_bits)&L2_idx_mask;
	long L2_tag_mask = (1<<L2_tag_bits)-1;
	(*l1_l2_info).l2_tag = (address>>(L2_offset_bits + L2_idx_bits))&L2_tag_mask;
	(*l1_l2_info).l2_assoc = l2_params.asociativity;

	if (L1_offset_bits + L1_idx_bits + L1_tag_bits == ADDRSIZE && L2_offset_bits + L2_idx_bits + L2_tag_bits == ADDRSIZE)
	{
		return OK;

	} else
	{
		return ERROR;
	}
}


int lru_replacement_policy_l1_l2(const entry_info* l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug) 
{
	

	/////Cache L1//////
	int tag_L1 = (*l1_l2_info).l1_tag;
	int associativity_L1 = (*l1_l2_info).l1_assoc;
	///////////////////

	/////Cache L2//////
	int tag_L2 = (*l1_l2_info).l2_tag;
	int associativity_L2 = (*l1_l2_info).l2_assoc;
	///////////////////

	//////////////////Revisa la caché L1////////////////////

	bool foundHit_L1 = 0;
	bool foundMiss_L1 = 0;

	bool foundHit_L2 = 0;
	bool foundMiss_L2 = 0;

	for (int i = 0; i < associativity_L1; i++){
		
		if (!l1_cache_blocks[i].valid){	//Si se llega a un dato invalido
			
			
			/////////////////////////MISS DE LECTURA (SET NO ESTÁ LLENO)/////////////////////////////////
			if (!loadstore){
				foundMiss_L1=1;
				(*l1_result).miss_hit = MISS_LOAD;
				(*l1_result).dirty_eviction = 0;
				//////////////////Escribir en el cache el dato que voy a leer (Se trae de memoria) 
				l1_cache_blocks[i].valid = 1;								
				l1_cache_blocks[i].tag = tag_L1;
				l1_cache_blocks[i].rp_value=0;
				l1_cache_blocks[i].dirty = 0; //Creo que así porque viene de un read
				///////////////
				for (int e = 0; e < i; e++){
					l1_cache_blocks[e].rp_value++;
				}
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			////////////////////////MISS DE ESCRITURA (SET NO ESTÁ LLENO)/////////////////////////////////
			else{
				foundMiss_L1=1;
				(*l1_result).miss_hit = MISS_STORE;
				(*l1_result).dirty_eviction = 0;
				//////////////////Escribir en el cache el dato
				l1_cache_blocks[i].valid = 1;
				l1_cache_blocks[i].tag = tag_L1;
				l1_cache_blocks[i].rp_value = 0;
				l1_cache_blocks[i].dirty = 1;
				////////////////////////////
				////Actualizar LRU
				for (int e = 0; e < i; e++){
					l1_cache_blocks[e].rp_value++;
				}
			}
			i=associativity_L1;	//Terminar el for y no sigue iterando
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
		}
		
		else{	//Si el dato no es invalido
			
			
			////////////////////////////////////////HIT DE LECTURA////////////////////////////////////////////
			if (l1_cache_blocks[i].tag == tag_L1 && !loadstore){	
				foundHit_L1=1;
				(*l1_result).miss_hit = HIT_LOAD;
				////Actualizar LRU
				for (int e = 0; e < associativity_L1; e++){
					if (l1_cache_blocks[e].valid==1){
						if (i!=e && l1_cache_blocks[e].rp_value<l1_cache_blocks[i].rp_value){
							l1_cache_blocks[e].rp_value++;
						}
					}else{
						e=associativity_L1;
					}
				}
				l1_cache_blocks[i].rp_value = 0;
				////////////////
				i = associativity_L1;

				/////////////////////También hit en L2//////////////////
				foundHit_L2=1;
				(*l2_result).miss_hit = HIT_LOAD;
				////Actualizar LRU
				for (int e = 0; e < associativity_L2; e++){
					if (l2_cache_blocks[e].valid==1){
						if (i!=e && l2_cache_blocks[e].rp_value<l2_cache_blocks[i].rp_value){
							l2_cache_blocks[e].rp_value++;
						}
					}else{
						e=associativity_L2;
					}
				}
				l2_cache_blocks[i].rp_value = 0;
				////////////////
				i = associativity_L2;

				return OK;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			////////////////////////////////////////HIT DE ESCRITURA////////////////////////////////////////////
			else if (l1_cache_blocks[i].tag == tag_L1 && loadstore){ 
				foundHit_L1=1;
				(*l1_result).miss_hit = HIT_STORE;
				l1_cache_blocks[i].dirty = 1;
				////Actualizar LRU
				for (int e = 0; e < associativity_L1; e++){
					if (l1_cache_blocks[e].valid){
						if (i!=e && l1_cache_blocks[e].rp_value<l1_cache_blocks[i].rp_value){
							l1_cache_blocks[e].rp_value++;
						}
					}else{
						e=associativity_L1;
					}
				}
				l1_cache_blocks[i].rp_value = 0;
				///////////////////////
				i = associativity_L1;
				/////////////////////También hit en L2//////////////////
				foundHit_L2=1;
				(*l2_result).miss_hit = HIT_STORE;
				l2_cache_blocks[i].dirty = 1;
				////Actualizar LRU
				for (int e = 0; e < associativity_L2; e++){
					if (l2_cache_blocks[e].valid){
						if (i!=e && l2_cache_blocks[e].rp_value<l2_cache_blocks[i].rp_value){
							l2_cache_blocks[e].rp_value++;
						}
					}else{
						e=associativity_L2;
					}
				}
				l2_cache_blocks[i].rp_value = 0;
				///////////////////////
				i = associativity_L2;

				return OK;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			/////////////////////////////////MISS DE LECTURA(SET LLENO)///////////////////////////////////////
			else if (!loadstore && !foundHit_L1 && !foundMiss_L1 && i==associativity_L1-1){	//Lectura en Miss: victimizar
				foundMiss_L1=1;
				(*l1_result).miss_hit = MISS_LOAD;
				for (int e = 0; e < associativity_L1; e++){	//Cual victimizo? -> El que tenga LRU= assosiativity-1
					if (l1_cache_blocks[e].rp_value==associativity_L1-1){
						(*l1_result).evicted_address = l1_cache_blocks[e].tag;//Victimizar ese e
						if (l1_cache_blocks[e].dirty){
							(*l1_result).dirty_eviction = 1; 	//Escribir a memoria
						}else{
							(*l1_result).dirty_eviction = 0;
						}
						//////////////////Escribir en el cache el dato que ocupo leer
						l1_cache_blocks[e].tag = tag_L1;
						l1_cache_blocks[e].rp_value=0;
						l1_cache_blocks[e].dirty = 0; //Creo que así porque viene de un read
						///////////////
					}else{
						l1_cache_blocks[e].rp_value++;	//Actualizar LRUs
					}
				}
				i = associativity_L1;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			/////////////////////////////////MISS DE ESCRITURA(SET LLENO)///////////////////////////////////////
			else if (loadstore && !foundHit_L1 && !foundMiss_L1 && i==associativity_L1-1){	//Escritura en Miss: victimizar
				foundMiss_L1=1;
				(*l1_result).miss_hit = MISS_STORE;
				for (int e = 0; e < associativity_L1; e++){	//Cual victimizo? -> El que tenga LRU= assosiativity-1
					if (l1_cache_blocks[e].rp_value==associativity_L1-1){
						(*l1_result).evicted_address = l1_cache_blocks[e].tag;	
						if (l1_cache_blocks[e].dirty){
							(*l1_result).dirty_eviction = 1;	//Escribir a memoria
						}else{
							(*l1_result).dirty_eviction = 0;
						}
						///////////////////Sobreescribir el dato en cache
						l1_cache_blocks[e].tag = tag_L1;
						l1_cache_blocks[e].rp_value = 0;
						l1_cache_blocks[e].dirty = 1;
						//////////////////////////////////////////////
					}else{
						l1_cache_blocks[e].rp_value++;	//Actualizar LRUs
					}
				}
				i = associativity_L1;
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////	
		}
		
	}


	//////////////////Revisa la caché L2////////////////////

	for (int i = 0; i < associativity_L2; i++){
		
		if (!l2_cache_blocks[i].valid){	//Si se llega a un dato invalido
			
			
			/////////////////////////MISS DE LECTURA (SET NO ESTÁ LLENO)/////////////////////////////////
			if (!loadstore){
				foundMiss_L2=1;
				(*l2_result).miss_hit = MISS_LOAD;
				(*l2_result).dirty_eviction = 0;
				//////////////////Escribir en el cache el dato que voy a leer (Se trae de memoria) 
				l2_cache_blocks[i].valid = 1;								
				l2_cache_blocks[i].tag = tag_L2;
				l2_cache_blocks[i].rp_value=0;
				l2_cache_blocks[i].dirty = 0; //Creo que así porque viene de un read
				///////////////
				for (int e = 0; e < i; e++){
					l2_cache_blocks[e].rp_value++;
				}
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			////////////////////////MISS DE ESCRITURA (SET NO ESTÁ LLENO)/////////////////////////////////
			else{
				foundMiss_L2=1;
				(*l2_result).miss_hit = MISS_STORE;
				(*l2_result).dirty_eviction = 0;
				//////////////////Escribir en el cache el dato
				l2_cache_blocks[i].valid = 1;
				l2_cache_blocks[i].tag = tag_L2;
				l2_cache_blocks[i].rp_value = 0;
				l2_cache_blocks[i].dirty = 1;
				////////////////////////////
				////Actualizar LRU
				for (int e = 0; e < i; e++){
					l2_cache_blocks[e].rp_value++;
				}
			}
			i=associativity_L2;	//Terminar el for y no sigue iterando
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
		}
		
		else{	//Si el dato no es invalido
			
			
			////////////////////////////////////////HIT DE LECTURA////////////////////////////////////////////
			if (l2_cache_blocks[i].tag == tag_L2 && !loadstore){	
				foundHit_L2=1;
				(*l2_result).miss_hit = HIT_LOAD;
				////Actualizar LRU
				for (int e = 0; e < associativity_L2; e++){
					if (l2_cache_blocks[e].valid==1){
						if (i!=e && l2_cache_blocks[e].rp_value<l2_cache_blocks[i].rp_value){
							l2_cache_blocks[e].rp_value++;
						}
					}else{
						e=associativity_L2;
					}
				}
				l2_cache_blocks[i].rp_value = 0;
				////////////////
				i = associativity_L2;
				return OK;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			////////////////////////////////////////HIT DE ESCRITURA////////////////////////////////////////////
			else if (l2_cache_blocks[i].tag == tag_L2 && loadstore){ 
				foundHit_L2=1;
				(*l2_result).miss_hit = HIT_STORE;
				l2_cache_blocks[i].dirty = 1;
				////Actualizar LRU
				for (int e = 0; e < associativity_L2; e++){
					if (l2_cache_blocks[e].valid){
						if (i!=e && l2_cache_blocks[e].rp_value<l2_cache_blocks[i].rp_value){
							l2_cache_blocks[e].rp_value++;
						}
					}else{
						e=associativity_L2;
					}
				}
				l2_cache_blocks[i].rp_value = 0;
				///////////////////////
				i = associativity_L2;
				return OK;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			/////////////////////////////////MISS DE LECTURA(SET LLENO)///////////////////////////////////////
			else if (!loadstore && !foundHit_L2 && !foundMiss_L2 && i==associativity_L2-1){	//Lectura en Miss: victimizar
				foundMiss_L2=1;
				(*l2_result).miss_hit = MISS_LOAD;
				for (int e = 0; e < associativity_L2; e++){	//Cual victimizo? -> El que tenga LRU= assosiativity-1
					if (l2_cache_blocks[e].rp_value==associativity_L2-1){
						(*l2_result).evicted_address = l2_cache_blocks[e].tag;//Victimizar ese e
						if (l2_cache_blocks[e].dirty){
							(*l2_result).dirty_eviction = 1; 	//Escribir a memoria
						}else{
							(*l2_result).dirty_eviction = 0;
						}
						//////////////////Escribir en el cache el dato que ocupo leer
						l2_cache_blocks[e].tag = tag_L2;
						l2_cache_blocks[e].rp_value=0;
						l2_cache_blocks[e].dirty = 0; //Creo que así porque viene de un read
						///////////////
					}else{
						l2_cache_blocks[e].rp_value++;	//Actualizar LRUs
					}
				}
				i = associativity_L2;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			/////////////////////////////////MISS DE ESCRITURA(SET LLENO)///////////////////////////////////////
			else if (loadstore && !foundHit_L2 && !foundMiss_L2 && i==associativity_L2-1){	//Escritura en Miss: victimizar
				foundMiss_L2=1;
				(*l2_result).miss_hit = MISS_STORE;
				for (int e = 0; e < associativity_L2; e++){	//Cual victimizo? -> El que tenga LRU= assosiativity-1
					if (l2_cache_blocks[e].rp_value==associativity_L2-1){
						(*l2_result).evicted_address = l2_cache_blocks[e].tag;	
						if (l2_cache_blocks[e].dirty){
							(*l2_result).dirty_eviction = 1;	//Escribir a memoria
						}else{
							(*l2_result).dirty_eviction = 0;
						}
						///////////////////Sobreescribir el dato en cache
						l2_cache_blocks[e].tag = tag_L2;
						l2_cache_blocks[e].rp_value = 0;
						l2_cache_blocks[e].dirty = 1;
						//////////////////////////////////////////////
					}else{
						l2_cache_blocks[e].rp_value++;	//Actualizar LRUs
					}
				}
				i = associativity_L2;
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////	
		}
		
	}

	return OK;

/*
	lru_L1 = lru_replacement_policy (idx_L1, tag_L1, associativity_L1, loadstore, l1_l1_cache_blocks[idx_L1], l1_result, debug);

	if ((*l1_result).miss_hit == hit_load || (*l1_result).miss_hit == hit_store)
	{
		lru_L2 = lru_replacement_policy (idx_L2, tag_L2, associativity_L2, loadstore, l2_l1_cache_blocks[idx_L2], l2_result, debug);
		return OK;
	}
	else
	{
		lru_L2 = lru_replacement_policy (idx_L2, tag_L2, associativity_L2, loadstore, l2_l1_cache_blocks[idx_L2], l2_result, debug);

		if ((*l1_result).miss_hit == hit_load || (*l1_result).miss_hit == hit_store)
		{
			return OK;
		}
		else
		{
			return OK;
		}
		
	}
	
*/
}
