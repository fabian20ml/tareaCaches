/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{
	(*field_size).offset = log2(cache_params.block_size);
	(*field_size).idx = log2((cache_params.size*KB)/(cache_params.block_size*cache_params.asociativity));
	(*field_size).tag = ADDRSIZE-(*field_size).offset-(*field_size).idx;

	if ((*field_size).offset+(*field_size).idx+(*field_size).tag == ADDRSIZE)
	{
		return OK;
	} else
	{
		return ERROR;
	}
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{
	long indexMASK = (1<<field_size.idx)-1;
	*idx = (address>>field_size.offset)&indexMASK;
	long tagMASK = (1<<field_size.tag)-1;
	*tag = (address>>(field_size.offset+field_size.idx))&tagMASK;
}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
	
	
	// REVISION DE PARAMETROS
	bool validAsoc = (associativity&(associativity-1));
	if (validAsoc || associativity<1 || idx<1 || tag<0)
	{
		return PARAM;
		}else{

		// VARIABLES MISCELANEAS
		bool foundHit,foundEmpty,maxRRPV = false;
		int M;
		int emptyWay,distantBlock;
		if (associativity <= 2) M = 1;
		else M = 2;
		// RRPV para victimizar
		uint8_t distantRRPV = (1<<M)-1; 
		// RPPV para inserciones
		uint8_t longRRPV = (1<<M)-2; 

		// LOGICA SRRIP HP
		// Recorrer el set buscando hit, via vacia y/o bloque con R-R distante.
		for (int i = 0; i < associativity; i++)
		{	// HIT
			if ( cache_blocks[i].valid && tag == cache_blocks[i].tag )
			{	// Hit promotion
				foundHit = true;
				cache_blocks[i].rp_value = 0;
				
				if (loadstore) 
				{// Write
					cache_blocks[i].dirty = 1;
					(*result).dirty_eviction = false;
					(*result).miss_hit = HIT_STORE;
				} else 
				{// Read	
					(*result).dirty_eviction = false;
					(*result).miss_hit = HIT_LOAD;
				}
				return OK;

			} else if (!cache_blocks[i].valid && !foundEmpty)
			{ // Guarda la via vacia
				emptyWay = i;
				foundEmpty = true;
			}
			if (cache_blocks[i].rp_value == distantRRPV && !maxRRPV)
			{	// Guarda la via del bloque distante
				distantBlock = i;
				maxRRPV = true;
			}
		}

		// El set no esta lleno (MISS)
		if (!foundHit && foundEmpty)
		{
			cache_blocks[emptyWay].valid = 1;
			cache_blocks[emptyWay].tag = tag;
			cache_blocks[emptyWay].rp_value = longRRPV;
			if (loadstore)
			{ // Write
				cache_blocks[emptyWay].dirty = 1;
				(*result).dirty_eviction = false;
				(*result).miss_hit = MISS_STORE;
			} else 
			{ // Read
				cache_blocks[emptyWay].dirty = 0;	
				(*result).dirty_eviction = false;
				(*result).miss_hit = MISS_LOAD;
			}
			
		
		} else if (!foundHit && !foundEmpty)
		{ // El set esta lleno (MISS)
			CHECKFORDISTANT: if (maxRRPV)
			{	
				if (cache_blocks[distantBlock].dirty) (*result).dirty_eviction = true; // WriteBack
				else (*result).dirty_eviction = false;
				(*result).evicted_address = cache_blocks[distantBlock].tag; // Bloque victimizado
				cache_blocks[distantBlock].valid = 1;
				cache_blocks[distantBlock].tag = tag;
				cache_blocks[distantBlock].rp_value = longRRPV;

				if (loadstore) 
				{ // Write
					cache_blocks[distantBlock].dirty = 1;
					(*result).miss_hit = MISS_STORE;
				}
				else 
				{ // Read
					cache_blocks[distantBlock].dirty = 0;	
					(*result).miss_hit = MISS_LOAD;
				}
			} else
			{
				for (int i = 0; i < associativity; i++)
				{
					cache_blocks[i].rp_value++;
					if (!maxRRPV && cache_blocks[i].rp_value==distantRRPV)
					{
						maxRRPV = true;
						distantBlock = i;
					}
				}
				goto CHECKFORDISTANT;
			}
		} 
		return OK;
	}
}


int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
	bool foundHit = 0;
	bool foundMiss = 0;
if (idx < 0 || tag < 0 || associativity <= 0)
{
	return PARAM;
}else{
	for (int i = 0; i < associativity; i++){
		
		if (!cache_blocks[i].valid){	//Si se llega a un dato invalido
			
			
			/////////////////////////MISS DE LECTURA (SET NO ESTÁ LLENO)/////////////////////////////////
			if (!loadstore){
				foundMiss=1;
				(*result).miss_hit = MISS_LOAD;
				(*result).dirty_eviction = 0;
				//////////////////Escribir en el cache el dato que voy a leer (Se trae de memoria) 
				cache_blocks[i].valid = 1;								
				cache_blocks[i].tag = tag;
				cache_blocks[i].rp_value=0;
				cache_blocks[i].dirty = 0; //Creo que así porque viene de un read
				///////////////
				for (int e = 0; e < i; e++){
					cache_blocks[e].rp_value++;
				}
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			////////////////////////MISS DE ESCRITURA (SET NO ESTÁ LLENO)/////////////////////////////////
			else{
				foundMiss=1;
				(*result).miss_hit = MISS_STORE;
				(*result).dirty_eviction = 0;
				//////////////////Escribir en el cache el dato
				cache_blocks[i].valid = 1;
				cache_blocks[i].tag = tag;
				cache_blocks[i].rp_value = 0;
				cache_blocks[i].dirty = 1;
				////////////////////////////
				////Actualizar LRU
				for (int e = 0; e < i; e++){
					cache_blocks[e].rp_value++;
				}
			}
			i=associativity;	//Terminar el for y no sigue iterando
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
		}
		
		else{	//Si el dato no es invalido
			
			
			////////////////////////////////////////HIT DE LECTURA////////////////////////////////////////////
			if (cache_blocks[i].tag == tag && !loadstore){	
				foundHit=1;
				(*result).miss_hit = HIT_LOAD;
				////Actualizar LRU
				for (int e = 0; e < associativity; e++){
					if (cache_blocks[e].valid==1){
						if (i!=e && cache_blocks[e].rp_value<cache_blocks[i].rp_value){
							cache_blocks[e].rp_value++;
						}
					}else{
						e=associativity;
					}
				}
				cache_blocks[i].rp_value = 0;
				////////////////
				i = associativity;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			////////////////////////////////////////HIT DE ESCRITURA////////////////////////////////////////////
			else if (cache_blocks[i].tag == tag && loadstore){ 
				foundHit=1;
				(*result).miss_hit = HIT_STORE;
				cache_blocks[i].dirty = 1;
				////Actualizar LRU
				for (int e = 0; e < associativity; e++){
					if (cache_blocks[e].valid){
						if (i!=e && cache_blocks[e].rp_value<cache_blocks[i].rp_value){
							cache_blocks[e].rp_value++;
						}
					}else{
						e=associativity;
					}
				}
				cache_blocks[i].rp_value = 0;
				///////////////////////
				i = associativity;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			/////////////////////////////////MISS DE LECTURA(SET LLENO)///////////////////////////////////////
			else if (!loadstore && !foundHit && !foundMiss && i==associativity-1){	//Lectura en Miss: victimizar
				foundMiss=1;
				(*result).miss_hit = MISS_LOAD;
				for (int e = 0; e < associativity; e++){	//Cual victimizo? -> El que tenga LRU= assosiativity-1
					if (cache_blocks[e].rp_value==associativity-1){
						(*result).evicted_address = cache_blocks[e].tag;//Victimizar ese e
						if (cache_blocks[e].dirty){
							(*result).dirty_eviction = 1; 	//Escribir a memoria
						}else{
							(*result).dirty_eviction = 0;
						}
						//////////////////Escribir en el cache el dato que ocupo leer
						cache_blocks[e].tag = tag;
						cache_blocks[e].rp_value=0;
						cache_blocks[e].dirty = 0; //Creo que así porque viene de un read
						///////////////
					}else{
						cache_blocks[e].rp_value++;	//Actualizar LRUs
					}
				}
				i = associativity;
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			/////////////////////////////////MISS DE ESCRITURA(SET LLENO)///////////////////////////////////////
			else if (loadstore && !foundHit && !foundMiss && i==associativity-1){	//Escritura en Miss: victimizar
				foundMiss=1;
				(*result).miss_hit = MISS_STORE;
				for (int e = 0; e < associativity; e++){	//Cual victimizo? -> El que tenga LRU= assosiativity-1
					if (cache_blocks[e].rp_value==associativity-1){
						(*result).evicted_address = cache_blocks[e].tag;	
						if (cache_blocks[e].dirty){
							(*result).dirty_eviction = 1;	//Escribir a memoria
						}else{
							(*result).dirty_eviction = 0;
						}
						///////////////////Sobreescribir el dato en cache
						cache_blocks[e].tag = tag;
						cache_blocks[e].rp_value = 0;
						cache_blocks[e].dirty = 1;
						//////////////////////////////////////////////
					}else{
						cache_blocks[e].rp_value++;	//Actualizar LRUs
					}
				}
				i = associativity;
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
		}
		
	}
	return OK;
}
}



int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug)
{

if (idx < 0 || tag < 0 || associativity <= 0)
{
	return PARAM;
}else{

int cntZeros = 0;

      /////Busca un hit/////
for (int i = 0; i < associativity; i++)
{
	
   //////Verifica si es invalido///////
   if (!cache_blocks[i].valid)
    {
		
       for (int j = i; j < associativity; j++)
       {
          cache_blocks[j].rp_value = 1; ///Setea las demás vías en nru 1
       }
            ///LOAD///
          if (!loadstore)
          {	
            cache_blocks[i].tag = tag;
            cache_blocks[i].valid = 1;
            cache_blocks[i].rp_value = 0;
            cache_blocks[i].dirty = 0;
            (*operation_result).miss_hit = MISS_LOAD;
            return OK;

          }else ///STORE///
          {	
            cache_blocks[i].tag = tag;
            cache_blocks[i].valid = 1;
            cache_blocks[i].rp_value = 0;
            cache_blocks[i].dirty = 1;
            (*operation_result).miss_hit = MISS_STORE;
            return OK;
          }
          
    } else ////Verifica si hay un hit////
    {    
               //////////HIT LOAD//////////
          if (cache_blocks[i].tag == tag && !loadstore)
          {	
             cache_blocks[i].rp_value = 0;
             (*operation_result).miss_hit = HIT_LOAD;
             return OK;
          }

               //////////HIT STORE//////////
           if (cache_blocks[i].tag == tag && loadstore)
          {	
             cache_blocks[i].dirty = 1;
             cache_blocks[i].rp_value = 0;
             (*operation_result).miss_hit = HIT_STORE;
             return OK;
          }
    
    }
}
/////Busca el primer uno en un miss///// 
for (int i = 0; i < associativity; i++)
{ 	
   if (cache_blocks[i].rp_value == 1)
   {
	   
               //////////MISS LOAD//////////
     if (cache_blocks[i].tag != tag && !loadstore)
          { 
             if (!cache_blocks[i].dirty)
            {
               (*operation_result).dirty_eviction = 0;
            }
            else
            {
               (*operation_result).dirty_eviction = 1;
            }

            (*operation_result).evicted_address = cache_blocks[i].tag;
            cache_blocks[i].tag = tag;
            cache_blocks[i].rp_value = 0;
            cache_blocks[i].dirty = 0;
            (*operation_result).miss_hit = MISS_LOAD;
            return OK;
          }
         
               //////////MISS STORE//////////
          if (cache_blocks[i].tag != tag && loadstore)
          { 
             if (!cache_blocks[i].dirty)
            {
               (*operation_result).dirty_eviction = 0;
            }
            else
            {
               (*operation_result).dirty_eviction = 1;
            }

             (*operation_result).evicted_address = cache_blocks[i].tag;
             cache_blocks[i].tag = tag;
             cache_blocks[i].rp_value = 0;
             cache_blocks[i].dirty = 1;
             (*operation_result).miss_hit = MISS_STORE;
             return OK;
          }
   }
   else
   {
	
    cntZeros++;
   }
}
              
////Verfica si todas las vías fueron cero en un miss//// 
if (cntZeros == associativity)
{
   for (int i = 0; i < associativity; i++)
   {
       cache_blocks[i].rp_value = 1;
   }
               //////////MISS LOAD//////////
     if (cache_blocks[0].tag != tag && !loadstore)
          { 
             if (!cache_blocks[0].dirty)
            {
               (*operation_result).dirty_eviction = 0;
            }
            else
            {
               (*operation_result).dirty_eviction = 1;
            }

            (*operation_result).evicted_address = cache_blocks[0].tag;
            cache_blocks[0].tag = tag;
            cache_blocks[0].rp_value = 0;
            cache_blocks[0].dirty = 0;
            (*operation_result).miss_hit = MISS_LOAD;
            return OK;
          }
         
               //////////MISS STORE//////////
          if (cache_blocks[0].tag != tag && loadstore)
          { 
             if (!cache_blocks[0].dirty)
            {
               (*operation_result).dirty_eviction = 0;
            }
            else
            {
               (*operation_result).dirty_eviction = 1;
            }

             (*operation_result).evicted_address = cache_blocks[0].tag;
             cache_blocks[0].tag = tag;
             cache_blocks[0].rp_value = 0;
             cache_blocks[0].dirty = 1;
             (*operation_result).miss_hit = MISS_STORE;
             return OK;
          }

}

}
}
