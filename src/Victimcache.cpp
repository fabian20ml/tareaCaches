/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	bool loadstore,
       	                    	entry* l1_cache_blocks,
       	                  	 	entry* vc_cache_blocks,
        	                 	operation_result* l1_result,
              	              	operation_result* vc_result,
                	         	bool debug)
{
	//PARSE ARGUMENTS
	int indexL1 = (*l1_vc_info).l1_idx;
	int tagL1 = (*l1_vc_info).l1_tag;
	int asocL1 = (*l1_vc_info).l1_assoc;
	int asocVC = (*l1_vc_info).vc_assoc;
	int newIntoQueue;
	bool foundHit,foundEmpty = false;

	// LLAMADO A LRU POLICY
	int returnOfLRU = lru_replacement_policy(indexL1,
											tagL1,
											asocL1,
											loadstore,
											l1_cache_blocks,
											l1_result);

	// CASO ESPECIAL: MISS porque cache todavia no esta llena (VC no deberia hacer nada)
	for (int i = 0; i < asocL1; i++)
	{
		if (!l1_cache_blocks[i].valid)
		{
			return returnOfLRU;
		}
	}
	
	// Revisar LRU return y/o si hubo HIT en L1
	if (returnOfLRU == ERROR || returnOfLRU == PARAM || (*l1_result).miss_hit == HIT_LOAD || (*l1_result).miss_hit == HIT_STORE)
	{
		return returnOfLRU;
	} else
	{ // VICTIM CACHE 
		// Revisar HIT en VictimCache
		for (int i = 0; i < asocVC; i++)
		{	// Hay Hit
			if (tagL1 == vc_cache_blocks[i].tag && vc_cache_blocks[i].valid && !foundHit)
			{
				foundHit = true;
				if (loadstore) (*vc_result).miss_hit = HIT_STORE;
				else (*vc_result).miss_hit = HIT_LOAD;
				vc_cache_blocks[i].tag = (*l1_result).evicted_address;
				vc_cache_blocks[i].dirty = (*l1_result).dirty_eviction;
				vc_cache_blocks[i].rp_value = 0;
				newIntoQueue = i;
				goto UPDATE;
			
			// El VC no esta lleno
			} else if (!foundEmpty && !vc_cache_blocks[i].valid)
			{
				foundEmpty = true;
				newIntoQueue = i;
			}
		}

		if (foundEmpty)
		{
			if (loadstore) (*vc_result).miss_hit = MISS_STORE;
			else (*vc_result).miss_hit = MISS_LOAD;
			vc_cache_blocks[newIntoQueue].valid = true;
			vc_cache_blocks[newIntoQueue].tag = (*l1_result).evicted_address;
			vc_cache_blocks[newIntoQueue].dirty = (*l1_result).dirty_eviction;
			vc_cache_blocks[newIntoQueue].rp_value = 0;
		}

		UPDATE: for (int i = 0; i < asocVC; i++)
		{
			if (i != newIntoQueue && vc_cache_blocks[i].rp_value < asocVC-1 && vc_cache_blocks[i].valid)
			{	// si hubo hit -> no hay eviction
				vc_cache_blocks[i].rp_value++;
			} else if (vc_cache_blocks[i].rp_value == asocVC-1 && !foundHit)
			{	// si hubo miss -> hay que sacar el bloque first in y meter el nuevo evicted de L1
				if (loadstore) (*vc_result).miss_hit = MISS_STORE;
				else (*vc_result).miss_hit = MISS_LOAD;
				(*vc_result).evicted_address = vc_cache_blocks[i].rp_value;
				(*vc_result).dirty_eviction = vc_cache_blocks[i].dirty;
				vc_cache_blocks[i].tag = (*l1_result).evicted_address;
				vc_cache_blocks[i].dirty = (*l1_result).dirty_eviction;
				vc_cache_blocks[i].rp_value = 0;
			}
			
		}
	
	}

	return OK;

}