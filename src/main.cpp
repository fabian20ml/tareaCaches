#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <string>
#include <L1cache.h>
#include <debug_utilities.h>


/* Helper funtions */
void print_usage (cache_params Params, int missR, int missW, int hitR, int hitW, float MissRate, long CPU_time, long AMAT, int dirty_evictions)
{
	std::cout<< "_________________________________________________________________\n";
	std::cout<<"Cache Parameters \n";
	std::cout<< "_________________________________________________________________\n";
	std::cout<<"Cache Size (KB):" << '\t' << '\t' << '\t' << Params.size << '\n';
	std::cout<<"Cache Associativity:" << '\t' << '\t' << '\t' << Params.asociativity << '\n';
	std::cout<<"Cache Block Size (bytes):" << '\t'  << '\t' << Params.block_size << '\n';
	std::cout<< "_________________________________________________________________\n";
	std::cout<<"Simulation Results \n";
	std::cout<< "_________________________________________________________________\n";
	std::cout<<"CPU time (cycles):"<< '\t' << '\t' << CPU_time << '\n';
	std::cout<<"AMAT(cycles): "<< '\t' << '\t' << '\t' << AMAT << '\n';
	std::cout<<"Overall miss rate: "<< '\t' << '\t' << MissRate << '\n';
	std::cout<<"Dirty evictions: "<< '\t' << '\t' << dirty_evictions << '\n';
	std::cout<<"Load misses: "<< '\t' << '\t' << '\t' << missR << '\n';
	std::cout<<"Store misses: "<< '\t' << '\t' << '\t'<< missW << '\n';
	std::cout<<"Total misses: "<< '\t' << '\t' << '\t' << missR+missW << '\n';
	std::cout<<"Load hits: "<< '\t' << '\t' << '\t' << hitR << '\n';	
	std::cout<<"Store hits: "<< '\t' << '\t' << '\t' << hitW << '\n';	
	std::cout<<"Total hits: "<< '\t' << '\t' << '\t' << hitR+hitW << '\n';	

	exit (0);
}

int main(int argc, char * argv []) {

  	/* Parse argruments */
	int RP,IC,gotFieldSize,index,tag,returnStatus,numOfSets;
	int HitRead = 0,HitWrite = 0,MissRead = 0,MissWrite = 0;
	long direccionHex;
	char numeral;
	std::string direccionStr;
	bool LS;
	cache_params CacheParams1;
	cache_field_size FieldSize1;
	int dirty_evictions=0;
	long IC_t=0;
	long CPU_time;
	float MissRate;
	float AMAT;

	for (int i = 0; i < argc; i++){
		if (argv[i][0] == '-' && argv[i][1] == 't'){
		CacheParams1.size = atoi(argv[i+1]);
		}
		if (argv[i][0] == '-' && argv[i][1] == 'l'){
		CacheParams1.block_size = atoi(argv[i+1]);
		}
		if (argv[i][0] == '-' && argv[i][1] == 'a'){
		CacheParams1.asociativity = atoi(argv[i+1]);
		}
		if (argv[i][0] == '-' && argv[i][1] == 'r' && argv[i][2] == 'p'){
		RP = atoi(argv[i+1]);
		}
  	}

	gotFieldSize = field_size_get(CacheParams1,&FieldSize1);
	numOfSets = 1<<FieldSize1.idx;

	// Creacion de la Cache
	entry **CacheEntries = new entry*[numOfSets]; 
	for (int count = 0; count < numOfSets; ++count){
		CacheEntries[count] = new entry[CacheParams1.asociativity]; 
	}

	operation_result* result = new operation_result;
	
  	/* Get trace's lines and start your simulation */
	while (std::cin >> numeral >> LS >> direccionStr >> IC)
	{
		direccionHex = stol(direccionStr, 0, 16); 
		address_tag_idx_get(direccionHex,FieldSize1,&index,&tag);
	
	//Llamado a la politica de reemplazo
		switch (RP)
		{
		case LRU:
			returnStatus = lru_replacement_policy(index,tag,CacheParams1.asociativity,LS,CacheEntries[index],result);
			break;

		case NRU:
			returnStatus = nru_replacement_policy(index,tag,CacheParams1.asociativity,LS,CacheEntries[index],result);
			break;

		case RRIP:
			returnStatus = srrip_replacement_policy(index,tag,CacheParams1.asociativity,LS,CacheEntries[index],result);
			break;
		
		default:
			return ERROR;
		}

	// Evaluacion de resultados
		switch ((*result).miss_hit)
		{
		case MISS_LOAD:
			MissRead++;
			if ((*result).dirty_eviction)
				dirty_evictions++;
			break;
			
		case MISS_STORE:
			MissWrite++;
			if ((*result).dirty_eviction)
				dirty_evictions++;
			break;

		case HIT_LOAD:
			HitRead++;
			break;

		case HIT_STORE:
			HitWrite++;
			break;
		
		default:
			return ERROR;
		}

		

	}
	
	MissRate= float(MissWrite+MissRead)/float(MissWrite+MissRead+HitWrite+HitRead);
	float MissReadRate = float(MissRead)/float(MissWrite+MissRead+HitWrite+HitRead);
	float AccessPerInstruction = float(MissWrite+MissRead+HitWrite+HitRead)/float(IC_t);
	//CPU_time = IC_t*(1+MissReadRate*AccessPerInstruction*20);//Solo los loas se toman en cuenta segun el enunciado y por eso se usa MissReadRate
	CPU_time=IC_t-(MissRead+MissWrite+HitRead+HitWrite)+MissRead*21+(MissWrite+HitRead+HitWrite); //Miss penalty=20
	AMAT = 1 + 20*MissRate;

  	/* Print cache configuration */

  	/* Print Statistics */

	print_usage(CacheParams1,MissRead,MissWrite,HitRead,HitWrite,MissRate,CPU_time,AMAT, dirty_evictions);

	return 0;
	
}
