/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_obl_replacement_policy (int idx,
                                int tag,
                                int associativity,
                                bool loadstore,
                                entry* cache_block,
								entry* cache_block_obl,
                                operation_result* operation_result_cache_block,
								operation_result* operation_result_cache_obl,
                                bool debug=false)
{	
	int found_data;
	enum miss_hit_status expected_miss;  
	expected_miss = loadstore ? MISS_STORE: MISS_LOAD;
	int i;
	lru_replacement_policy (idx, tag, associativity, loadstore, cache_block, operation_result_cache_block, debug);
	for (int i = 0; i < associativity; i++){
		if(cache_block[i].tag == tag){
			found_data=i;
			i=associativity;
		}
	}
	if ((*operation_result_cache_block).miss_hit == expected_miss){
		cache_block[found_data].obl_tag = 0;
		lru_replacement_policy (idx, tag, associativity, loadstore=0, cache_block_obl, operation_result_cache_obl, debug);
		//~ if ((*operation_result_cache_obl).miss_hit == MISS_LOAD){
			for (int i = 0; i < associativity; i++){
				if(cache_block_obl[i].tag == tag){
					found_data=i;
					i=associativity;
				}
			}
			cache_block_obl[found_data].obl_tag = 1;
		//~ }
	}
	else if((*operation_result_cache_block).miss_hit != expected_miss && cache_block[found_data].obl_tag == 1){
		cache_block[found_data].obl_tag = 0;
		lru_replacement_policy (idx, tag, associativity, loadstore=0, cache_block_obl, operation_result_cache_obl, debug);
		//////////////////////Si no pasa la prueba borrar el if
		//~ if ((*operation_result_cache_obl).miss_hit == MISS_LOAD){
			for (int i = 0; i < associativity; i++){
				if(cache_block_obl[i].tag == tag){
					found_data=i;
					i=associativity;
				}
			}
			cache_block_obl[found_data].obl_tag = 1;
		//~ }
		///////////////////////		
	}
	
	return OK;
}
