/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <stdio.h>
#include <stdlib.h>



class L1cache : public ::testing::Test{
    protected:
		int debug_on;
		virtual void SetUp()
		{
			   /* Parse for debug env variable */
		   get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug_on = 1;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1<<((rand()%2)+1);
  //~ printf("associativity: %d\n", associativity); 
  
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];			
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
                                     
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug_on = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 + (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = associativity-1-i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = 0;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
	int associativity;
	int tagA;
	int idx;
	int i=0;
	int tag;
	idx = rand()%1024;
	//Choose a random associativity
	associativity = 1<<((rand()%2)+1);
	bool debug_on = 0;
	entry cache_line[associativity];
	bool loadstore = 1;
	operation_result result = {};
	//Choose a random policy 	
	int num = rand()%3;
	//~ printf("num: %d\n", num);   
	if (num==0){
		//Fill a cache entry
		for (i =  0; i < associativity; i++) {
			cache_line[i].valid = true;
			cache_line[i].tag = 1000+i;
			cache_line[i].dirty = 0;
			cache_line[i].rp_value = associativity-1-i;
		}
		//Insert a new block A
		tagA = cache_line[associativity-1].tag;
		//Force a hit on A
		lru_replacement_policy(idx, 
                                     tagA, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
        //Check rp_value of block A    
        //~ printf("RP: %d\n", cache_line[associativity-1].rp_value);                      
		EXPECT_EQ(cache_line[associativity-1].rp_value, 0);
		
		//Keep inserting new blocks until A is evicted
		for (i =  0; i < associativity; i++) {
			tag = 2000+i;
			lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
		}
		
		//Check eviction of block A happen after N new blocks were inserted
		EXPECT_EQ(result.evicted_address, tagA);
		
	}
	
	
	else if (num==1){
		//Fill a cache entry
		for (i =  0; i < associativity; i++) {
			cache_line[i].valid = true;
			cache_line[i].tag = 1000+i;
			cache_line[i].dirty = 0;
			cache_line[i].rp_value = 0;		//CAMBIAR ESTO DE ACUERDO A NRU
		}
		//Insert a new block A
		tagA = cache_line[associativity-1].tag;
		//Force a hit on A
		nru_replacement_policy(idx, 
                          tagA, 
                          associativity,
                          loadstore,
                          cache_line,
                          &result,
                          bool(debug_on));
        
        //Check rp_value of block A
                                
		EXPECT_EQ(cache_line[associativity-1].rp_value, 0);
		
		//Keep inserting new blocks until A is evicted
		for ( i =  0; i < associativity; i++) {		//Implementar esta parte para NRU
			tag = 2000+i;
			nru_replacement_policy(idx, 
                            tag, 
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
		 }
		
		//Check eviction of block A happen after N new blocks were inserted	
		EXPECT_EQ(result.evicted_address, tagA);
	}	
	
	else if (num==2){
		//Fill a cache entry
		for (i =  0; i < associativity; i++) {
			cache_line[i].valid = true;
			cache_line[i].tag = 1000+i;
			cache_line[i].dirty = 1;
			cache_line[i].rp_value = 0;
		}
		//Insert a new block A
		tagA = cache_line[associativity-1].tag;
		//Force a hit on A
		srrip_replacement_policy(idx, 
                                     tagA, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
        
        //Check rp_value of block A                             
		EXPECT_EQ(cache_line[associativity-1].rp_value, 0);
		
		//Keep inserting new blocks until A is evicted
		for ( i =  0; i < associativity; i++) {		//Implementar esta parte para SRRIP
		tag = 2000+i;
		srrip_replacement_policy(idx, 
                                    tag, 
                                    associativity,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
		}
		
		//Check eviction of block A happen after N new blocks were inserted	
		EXPECT_EQ(result.evicted_address, tagA);
	}
		
}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
	int associativity;
	int tagA, tagB, tag;
	int idx = rand()%1024;
	int i;
	//Choose a random associativity
	associativity = 1<<((rand()%2)+1);
	bool debug_on = 0;
	entry cache_line[associativity];
	bool loadstore = 1;
	operation_result result = {};
	//Choose a random policy
	int num = rand()%3;
	if (num==0){
		//Fill a cache entry with only read operations
		for (i =  0; i < associativity; i++) {
			cache_line[i].valid = true;
			cache_line[i].tag = 1000+i;
			cache_line[i].dirty = 0;
			cache_line[i].rp_value = associativity-1-i;
		}
		//Force a write hit for a random block A
		tagA=cache_line[associativity-1].tag;
		lru_replacement_policy(idx, 
                                     tagA, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
        //Force a read hit for a random block B
        loadstore = 0;                             
        tagB=cache_line[associativity-2].tag;
		lru_replacement_policy(idx, 
                                     tagB, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
                                     
        //Force a read hit for a random block A
		lru_replacement_policy(idx, 
                                     tagA, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
                                     
		//Insert lines until B is evicted
		for (i =  0; i < associativity-1; i++) {
			tag = 2000+i;
			lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
		}
		
		//Check dirty_bit for block B is false
		EXPECT_FALSE(result.dirty_eviction);
		
		//Insert lines until A is evicted
		tag = 2002+associativity;
		lru_replacement_policy(idx, 
                                    tag, 
                                    associativity,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
                                    
        //Check dirty_bit for block A is true
		EXPECT_TRUE(result.dirty_eviction);
	}
	
	
	else if (num==1){
		//Fill a cache entry with only read operations
		for (i =  0; i < associativity; i++) {
			cache_line[i].valid = true;
			cache_line[i].tag = 1000+i;
			cache_line[i].dirty = 0;
			cache_line[i].rp_value = 0;
		}
		//Force a write hit for a random block A
		tagA = cache_line[associativity-1].tag;
		nru_replacement_policy(idx, 
                            tagA, 
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));

        //Force a read hit for a random block B
        loadstore = 0;                             
        tagB=cache_line[associativity-1].tag-1;
		nru_replacement_policy(idx, 
                                     tagB, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
        
        //Force a read hit for a random block A
		nru_replacement_policy(idx, 
                                     tagA, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));                             
		//Insert lines until B is evicted
		 for (i =  0; i < associativity-1; i++) {
			tag = 2000+i;
			nru_replacement_policy(idx, 
                            tag, 
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
		}
		//Check dirty_bit for block B is true
		EXPECT_FALSE(result.dirty_eviction);
		
		//Insert lines until A is evicted
    
		tag = 3000;
		nru_replacement_policy(idx, 
                          tag, 
                          associativity,
                          loadstore,
                          cache_line,
                          &result,
                          bool(debug_on));
                              
        //Check dirty_bit for block B is false
		EXPECT_TRUE(result.dirty_eviction);
	
  }
	
	
	else if (num==2){

    
		//Fill a cache entry with only read operations
		for (i =  0; i < associativity; i++) {
			cache_line[i].valid = true;
			cache_line[i].tag = i;
			cache_line[i].dirty = 0;
			cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
		}
		//Force a write hit for a random block A
		tagA=cache_line[associativity-1].tag;
		srrip_replacement_policy(idx, 
                                     tagA, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    //Force a read hit for a random block B
    loadstore = 0;                         
      	tagB=cache_line[associativity-2].tag;
    
		srrip_replacement_policy(idx, 
                                     tagB, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
                                     
    //Force a read hit for a random block A
		srrip_replacement_policy(idx, 
                                     tagA, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
                                    
                                    
    while (result.evicted_address != tagB)
    {
      tag = rand()%4092+associativity;
			srrip_replacement_policy(idx, 
                                    tag, 
                                    associativity,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
		  
    }

		//Check dirty_bit for block B is true
		EXPECT_FALSE(result.dirty_eviction);


    //Insert lines until A is evicted
    while (result.evicted_address != tagA)
    {
      tag = rand()%4092+associativity;
			srrip_replacement_policy(idx, 
                                    tag, 
                                    associativity,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
		  
    }
		//~ //printf("DB:  %d", result.dirty_eviction);
		EXPECT_TRUE(result.dirty_eviction);

	}
}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
	/////////////////////////////////IDX NEGATIVO//////////////////////
	int idx = (rand()%1024)*-1;
	int tag = rand()%4096;
	int associativity = 1<<((rand()%2)+1);
	bool loadstore = 1;
	bool debug_on = 0;
	int status;
	operation_result result = {};
	entry cache_line[associativity];
	int num = rand()%3;;
	if (num==0){
		status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
	else if (num==1){
		status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
	else if (num==2){
		status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
	EXPECT_EQ(status, 1);
    
    
    ///////////////////TAG NEGATIVO////////////////////
    idx = rand()%1024;
	tag = (rand()%4096)*-1;
	associativity = 2 + (rand()%3);
	if (num==0){
		status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
	else if (num==1){
		status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
	else if (num==2){
		status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
    EXPECT_EQ(status, 1);
    
    /////////////////////////////////ASOCIATIVITY NEGATIVO//////////////////////
    idx = rand()%1024;
	tag = rand()%4096;
	associativity = (2 + (rand()%3))*-1;
	if (num==0){
		status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
	else if (num==1){
		status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
	else if (num==2){
		status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
	}
    EXPECT_EQ(status, 1);
	
}
